<?php

class ReviewController extends BaseController {

	public function create($courseid)
	{
		// dit gedeelte is klaar, hier hoeft u niets meer te wijzigen
		$data['courseid'] = $courseid;
		$data['success'] = Session::get('success');
		return View::make('review/create', $data);
	}

	public function handleCreate($courseid)
	{
		// dit gedeelte is klaar, hier hoeft u niets meer te wijzigen
		$input = Input::all();
		print_r($input);
		$r = new Review();
		$r->rating = $input['range'];
		$r->tips = $input['tips'];
		$r->course_id = $courseid; 
		$r->Save();

		return Redirect::to("review/create/$courseid")->with("success", "Bedankt voor de review!");
	}

	public function all()
	{
		$data['reviews'] = Review::all();
		return View::make('review/all', $data);
	}

	public function handleAll()
	{
		
	}

}
