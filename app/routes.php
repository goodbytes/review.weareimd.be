<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@Index');
Route::get('/review/create/{courseid}', 'ReviewController@Create');
Route::post('/review/create/{courseid}', 'ReviewController@handleCreate');

Route::get('/review/all', 'ReviewController@All');
Route::post('/review/all', 'ReviewController@HandleAll');