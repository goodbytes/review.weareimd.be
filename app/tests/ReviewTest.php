<?php

class ReviewTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */

	public function testHomePageShowCourses(){
		// the homepage should show a list of course where we can click
		// when clicked on a link, we should be redirected to /review/create/1 where 1 is the course ID (primary key)
		$this->call('GET', '/');
		$this->assertViewHas('courses');
	}

	public function testAllReviewsIsSecuredBySession()
	{
		// the controller at /review/all needs to pass a variable $reviews to the view but only when a user is logged in
		// make sure we cannot access this page without logging in first with password "iamimd"
		$input = array(
			'password' => 'iamimd'
		);
		Input::replace($input);
		$crawler = $this->client->request('POST', '/review/all', $input);
		$this->assertRedirectedTo('/review/all');
		$this->assertSessionHas('loggedin');
	}

	public function testApiReturnsAllReview()
	{
		// make sure the api at /api/reviews returns all review in JSON format
		$response = $this->call('GET', '/api/reviews');
		$this->assertTrue($this->client->getResponse()->isOk());
		$json = json_decode($response->getContent());
		$this->assertObjectHasAttribute("rating", $json[0]);
	}

}
