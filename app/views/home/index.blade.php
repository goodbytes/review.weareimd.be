<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reviews</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">

		<h1>Reviewr <span class="label label-warning">Beta</span></h1>

		<h3>Opgave</h3>
		<ul>
			<li>lees de tests in /app/tests/ReviewTest.php goed door</li>
			<li><strong>/</strong> moet alle courses uit de databank tonen en we moeten erop kunnen klikken om een nieuwe review te plaatsen voor de geselecteerde course (6p)</li>
			<li><strong>Reviews aanmaken</strong> werkt reeds, maar de werking hangt af van het vorige punt in deze lijst</li>
			<li><strong>/review/all</strong> moet vragen naar wachtwoord "iamimd" vooraleer alle reviews getoond worden, verberg het login veld na een succesvolle login (8p)</li>
			<li><strong>/api/reviews</strong> moet alle reviews tonen in geldig JSON formaat zoals gedefineerd in de unit test (4p)</li>
			<li>wijzig de tests niet, implementeer uw code zodat de tests slagen als u ze uitvoert</li>
		</ul>
		
			

	</div>

</body>
</html>